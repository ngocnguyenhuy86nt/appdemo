import java.awt.EventQueue;

import controller.LoginController;
import view.LoginView;

public class App {

	public static void main(String[] args)
	{
                // Day la diem chay dau tien cua app
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				LoginView view = new LoginView();
				LoginController controller = new LoginController(view);
				//hien thi man hinh login
				controller.showLoginView();
			}
		});
	}
}
