package controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
import dao.UserDao;
import entity.User;
import view.LoginView;
import view.StudentView;
import controller.StudentController;
public class LoginController {

	private UserDao userDao;
	private view.LoginView loginView;
	private StudentView studentView;
	
	public LoginController(LoginView _view)
	{
		this.loginView = _view;
		// load du lie user tu user.xml va luu vao List User
		this.userDao = new UserDao();
		_view.addLoginListener(new LoginListener());
	}
	
	public void showLoginView()
	{
		loginView.setVisible(true);
	}
	
	class LoginListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			User user =  loginView.getUser();
			// user: admin
			// pass: admin
			if(userDao.checkUser(user))
			{
				//neu dang nhap thanh cong, mo man hinh quan ly sinh vien
				studentView = new StudentView();
				StudentController studentController = new StudentController(studentView);
				studentController.showStudentView();
				loginView.setVisible(false);
			}
			else
			{
				loginView.showMessage("username hoac password khong dung.");
			}
					
		}

	}
	
}
