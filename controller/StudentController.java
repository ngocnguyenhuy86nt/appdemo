package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import dao.StudentDao;
import entity.Student;
import view.StudentView;

public class StudentController {

	private StudentDao studentDao;
	private StudentView studentView;

	public StudentController(StudentView studentView)
	{
		this.studentView = studentView;
		studentDao = new StudentDao();

		studentView.addAddStudentListener(new AddStudentListener());
		studentView.addEditStudentListener(new EditStudentListener());
		studentView.addDeleteStudentListener(new DeleteStudentListener());
		studentView.addClearStudentListener(new ClearStudentListener());
		studentView.addSortStudentGPAListener(new SortStudentGPAListener());
		studentView.addSortStudentNameListener(new SortStudentNameListener());
	}

	public void showStudentView()
	{
		List<Student> studentList = studentDao.getListStudents();
		studentView.setVisible(true);
		studentView.showListStudents(studentList);
	}


	/*Lop  noi AddStudentListener
	 * chua cai dat cho su kien click button "Add"
	 *
	 * */
	class AddStudentListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Student student = studentView.getStudentInfo();
			if(student != null)
			{
				studentDao.add(student);
				studentView.showStudent(student);
				studentView.showListStudents(studentDao.getListStudents());
				studentView.showMessage("Them Thanh Cong");
			}
		}

	}

	/*lop EditStudentListener chua cai dat cho su kien click button "Edit"
	 *
	 * */
	class EditStudentListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Student student = studentView.getStudentInfo();
			if(student != null)
			{
				studentDao.edit(student);
				studentView.showStudent(student);
				studentView.showListStudents(studentDao.getListStudents());
				studentView.showMessage("Cap nhat thanh cong");
			}
		}
	}

	/*lop DeleteStudentListener
	 * chua cai dat su kien click button ""Delete"
	 *
	 * */
	class DeleteStudentListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Student student = studentView.getStudentInfo();
			if(student != null)
			{
				studentDao.delete(student);
				studentView.clearStudentInfo();
				studentView.showListStudents(studentDao.getListStudents());
				studentView.showMessage("Xoa thanh cong");
			}
		}
	}

	/*Lop ClearStudentListener chua cai dat cho
	 * su kien click button "Clear"
	 * */
	class ClearStudentListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			studentView.clearStudentInfo();
		}
	}

	/*Lop SortStudentGPAListener chua cai dat cho su kien
	 * click button "Sort By GPA"
	 * */
	class SortStudentGPAListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			studentDao.sortStudentByGPA();
			studentView.showListStudents(studentDao.getListStudents());
		}
	}

	/*Lop SortStudentNameListener chua cai dat cho su kien
	 * click button "Sort By Name"
	 * */
	class SortStudentNameListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			studentDao.sortStudentByName();
			studentView.showListStudents(studentDao.getListStudents());
		}
	}

	/*Lop ListStudentSelectionListener
	 * chua cai dat cho su kien chon student trong bang student
	 * */
	class ListStudentSelectionListener implements ListSelectionListener
	{

		@Override
		public void valueChanged(ListSelectionEvent e) {
			// TODO Auto-generated method stub
			studentView.fillStudentFromSelectRow();
		}
	}
}
