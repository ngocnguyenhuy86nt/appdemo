package dao;

import java.util.ArrayList;
import java.util.List;

import entity.User;
import entity.UserXML;
import utils.FileUtils;

public class UserDao {

	private static final String USER_FILE_NAME = "./src/db/user.xml";
	private List<User> listUsers;
	
	public UserDao()
	{
		this.listUsers = readListUsers();
	}
	
	//doc cac doi tuong user tu file user.xml
	public List<User> readListUsers()
	{
		List<User> list = new ArrayList<User>();
		UserXML userXML = (UserXML) FileUtils.readXMLFile(USER_FILE_NAME, UserXML.class);
		if(userXML != null)
		{
			list = userXML.getUser();
		}
		return list;
	}
	public List<User> getListUsers() 
	{
        return listUsers;
    }
	
	public void setListSUsers(List<User> listUsers) 
	{
        this.listUsers = listUsers;
    }
    
	public boolean checkUser(User user)
	{
		if(user != null)
		{
			for(int i = 0; i < listUsers.size(); i++)
			{
				User user_temp = listUsers.get(i);
				
				// kiem tra username/password cua user nhap tu man hinh so voi user trong file user.xml
				if(user_temp.getUserName().equals(user.getUserName())
						&& user_temp.getPassword().equals(user.getPassword()))
					return true;
			}
		}
		return false;
	}
}
