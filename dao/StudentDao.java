package dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
 
import entity.Student;
import entity.StudentXML;
import utils.FileUtils;

public class StudentDao {

	private static final String STUDENT_FILE_NAME = "./src/db/student.xml";
	private List<Student> listStudents;
	
	public StudentDao()
	{
		this.listStudents = readListStudents();
	}
	
	//doc cac doi tuong student tu file student.xml
	public List<Student> readListStudents()
	{
		List<Student> list = new ArrayList<Student>();
		StudentXML studentXML = (StudentXML) FileUtils.readXMLFile(STUDENT_FILE_NAME, StudentXML.class);
		if(studentXML != null)
		{
			list = studentXML.getStudent();
		}
		return list;
	}
	
	// them student vao listStudent va luu listStudents vao file
	public void add(Student student)
	{
		int id = (listStudents.size() > 0) ? (listStudents.size() + 1) : 1;
		student.setId(id);
		listStudents.add(student);
		writeListStudents(listStudents);
	}
	
	// cap nhat student vao listStudent va luu listStudent vao file
	public void edit(Student student)
	{
		int size = listStudents.size();
		for(int i = 0; i < size; i++)
		{
			if(listStudents.get(i).getId() == student.getId())
			{
				listStudents.get(i).setName(student.getName());
				listStudents.get(i).setAge(student.getAge());
				listStudents.get(i).setAddress(student.getAddress());
				listStudents.get(i).setGpa(student.getGpa());
				writeListStudents(listStudents);
				break;
			}
		}
	}
	
	// xoa student tu listStudents va luu listStudents vao file
	public boolean delete(Student student)
	{
		boolean isFound = false;
		int size = listStudents.size();
		for(int i = 0; i < size; i++)
		{
			if(listStudents.get(i).getId() == student.getId())
			{
				student = listStudents.get(i);
				isFound = true;
				break;
			}
		}
		if(isFound)
		{
			listStudents.remove(student);
			writeListStudents(listStudents);
			return true;
		}
		return false;
	}
	
	//sap xep danh sach student theo name theo thu tu tang dan
	public void sortStudentByName()
	{
        Collections.sort(listStudents, new Comparator<Student>() {
            public int compare(Student student1, Student student2) {
                return student1.getName().compareTo(student2.getName());
            }
        });
    }
	
	//sap xep danh sach student theo GPA theo thu tu tang dan
	public void sortStudentByGPA()
	{
		Collections.sort(listStudents, new Comparator<Student>() {
            public int compare(Student student1, Student student2) {
                if (student1.getGpa() > student2.getGpa()) {
                    return 1;
                }
                return -1;
            }
        });
	}
	
	public List<Student> getListStudents() 
	{
        return listStudents;
    }
	
	public void setListStudents(List<Student> listStudents) 
	{
        this.listStudents = listStudents;
    }
    
	
	/* luu cac doi tuong student vao file student.xml
	 * *
	 */
	private void writeListStudents(List<Student> students) {
		// TODO Auto-generated method stub
		StudentXML studentXML = new StudentXML();
		studentXML.setStudent(students);
		FileUtils.writeXMLtoFile(STUDENT_FILE_NAME, studentXML);
	}
}





































