package entity;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class User implements Serializable {

	//private static final long serialVersiomUID = 1L;
	private String username; // dat ten bien username phai trung voi ten thuoc tinh cua user.xml
	private String password; // dat ten bien password phai trung voi ten thuoc tinh cua user.xml

	// phai them constructor khong doi so
	public User()
	{
		userName = "";
		password = "";
	}

	public User(String _username, String _password)
	{
		super();
		this.username = _username;
		this.password = _password;
	}

	public String getUserName()
	{
		return username;
	}

	public void setUserName(String username)
	{
		this.username = username;

	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
}
