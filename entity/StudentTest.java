package entity;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "student")
@XmlAccessorType(XmlAccessType.FIELD)

public class Student implements Serializable{

	private static final long serialVersiomUID = 1L;
	private int id;
	private String name;
	private byte age;
	private String address;
	//diem tb cua SV
	private float gpa;
	
	public Student()
	{
		
	}
	
	public Student(int _id, String _name, byte _age, String _address, float _gpa)
	{
		super();
		this.id = _id;
		this.name = _name;
		this.age = _age;
		this.address = _address;
		this.gpa = _gpa;
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int _id)
	{
		this.id = _id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String _name)
	{
		this.name = _name;
	}
	
	public byte getAge() 
	{
        return age;
    }
 
    public void setAge(byte _age) 
    {
        this.age = _age;
    }
 
    public String getAddress() 
    {
        return address;
    }
 
    public void setAddress(String _address) 
    {
        this.address = _address;
    }
 
    public float getGpa() {
        return gpa;
    }
 
    public void setGpa(float _gpa) 
    {
        this.gpa = _gpa;
    }
}










