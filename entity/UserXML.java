package entity;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "users")
@XmlAccessorType(XmlAccessType.FIELD)

public class UserXML {

	private List<User> user;
	
	public List<User> getUser()
	{
		return user;
	}
	
	public void setUser(List<User> _user)
	{
		this.user = _user;
	}
}