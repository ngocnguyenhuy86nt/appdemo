package view;

public class HomeView extends JFrame implements ActionListener
{

  public HomeView()
	{
		initComponents();
	}

  private void initComponents()
  {

  }

  public void showMessage(String message)
  {
    JOptionPane.showMessageDialog(this, message);
  }
}
