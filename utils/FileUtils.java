package utils;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;



public class FileUtils {
	
	
	/*
	 * Doc noi dung fileName, sau do chuyen doi noi dung cua file thanh
	 * doi tuong co kieu la clazz
	 * */
	public static Object readXMLFile(String fileName, Class<?> clazz)
	{
		try
		{
			File xmlFile = new File(fileName);
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			return jaxbUnmarshaller.unmarshal(xmlFile);
		}
		catch(JAXBException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/* chuyen doi doi tuong object ve dinh dang xml
	 * sau do luu vao fileName
	 * 
	 * @param fileName
	 * @param object
	 * */ 
	public static void writeXMLtoFile(String fileName, Object object)
	{
		try
		{
			//tao doi tuong JAXBContext
			JAXBContext  jaxbContext = JAXBContext.newInstance(object.getClass());
			
			//tao doi tuong Marshaller
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			
			//formating
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			//luu noi dung xml vao file
			File xmlFile = new File(fileName);
			jaxbMarshaller.marshal(object, xmlFile);
			
		}
		catch(JAXBException e)
		{
			e.printStackTrace();
		}
	}
	 
}
